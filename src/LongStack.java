import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;

/*
useful links
https://enos.itcollege.ee/~japoia/algoritmid/magpraktnew.html
https://www.geeksforgeeks.org/linkedlist-descendingiterator-method-in-java-with-examples/
https://www.geeksforgeeks.org/linked-list-in-java/
https://en.wikipedia.org/wiki/Reverse_Polish_notation
http://java-online.ru/blog-tokenizer.xhtml
 */

public class LongStack implements Cloneable{

   public static void main(String[] argum) {
      //System.out.println(LongStack.interpret("1  2    +"));
      //System.out.println(LongStack.interpret("2 15 -"));
      //System.out.println(LongStack.interpret("5 2 - 7"));
      //System.out.println(LongStack.interpret(null));
      //System.out.println(LongStack.interpret("    "));
      //System.out.println(LongStack.interpret("67 xxx +"));
      //System.out.println(LongStack.interpret("3 4 + - 5"));
   }

   private final LinkedList<Long> longLinkedList;

   LongStack() {
      this.longLinkedList = new LinkedList<>();
   }

   LongStack(LinkedList<Long> longLinkedList) {
      this.longLinkedList = longLinkedList;
   }

   @SuppressWarnings("unchecked")
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new LongStack((LinkedList<Long>) this.longLinkedList.clone());
   }

   public boolean stEmpty() {
      return this.longLinkedList.isEmpty();
   }

   public void push(long a) {
      this.longLinkedList.push(a);
   }

   public long pop() {
      if (stEmpty()) {
         throw new IndexOutOfBoundsException("stack underflow: " + this);
      }
      return this.longLinkedList.pop();
   }

   public void op(String s) {
      if (this.longLinkedList.size() < 2) {
         throw new IllegalArgumentException("too few elements for " + s);
      }
      long op2 = pop();
      long op1 = pop();
      switch (s) {
         case "+" -> push(op1 + op2);
         case "-" -> push(op1 - op2);
         case "*" -> push(op1 * op2);
         case "/" -> push(op1 / op2);
         default -> throw new IllegalArgumentException("invalid operation: " + s);
      }
   }

   public long tos() {
      if (stEmpty()) {
         throw new IndexOutOfBoundsException("stack underflow: " + this);
      }
      return this.longLinkedList.getFirst();
   }

   @Override
   public boolean equals(Object o) {
      if (getClass() != o.getClass()) {
         return false;
      }
      return this.longLinkedList.equals(((LongStack) o).longLinkedList);
   }

   @Override
   public String toString() {
      if (stEmpty()) {
         return "null";
      }
      StringBuilder string = new StringBuilder();
      Iterator<Long> x = longLinkedList.descendingIterator();
      while (x.hasNext()) {
         string.append(x.next());
         string.append(" ");
      }
      return string.toString();
   }

   public static long interpret(String pol) {
      if (pol == null || pol.trim().isEmpty()) {
         throw new IndexOutOfBoundsException("There are no Reverse Polish Notation for interpretation!\nGiven string: " + pol);
      }
      StringTokenizer tokenizer = new StringTokenizer(pol, " \t");
      LongStack longStack = new LongStack();
      long x;

      while (tokenizer.hasMoreElements()) {
         String token = (String) tokenizer.nextElement();

         if (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/")) {
            try {
               longStack.op(token);
            } catch (IllegalArgumentException exception) {
               throw new IllegalArgumentException("Not enough numbers to use with this operand: " + token + "\nGiven string: " + pol);
            }
         } else {
            try {
               x = Long.parseLong(token);
               longStack.push(x);
            } catch (NumberFormatException exception) {
               throw new NumberFormatException("The \"" + token + "\" symbol is not allowed\nGiven string: " + pol);
            }
         }
      }
      x = longStack.pop();
      if (!longStack.stEmpty()) {
         throw new RuntimeException("Not enough operands for this number: " + x + "\nGiven string: " + pol);
      }
      return x;
   }
}

